# QVDLandClim

Necessary file set to participate in the Lab using the model LandClim of the course Quantitative Vegetation Dynamics.

**Code**: Here you will find all the required R code scripts for running and analysing the output files

**Inputfiles**: These are the files that LandClim needs to run (a more detailed description can be found below)

**Manuals**: This document can be found in this folder

**Model**: The executable files of the LandClim model can be found here

**Outputfiles**: Here is where the output files will be storaged

**qvdlandclim.Rproj:** This is the RStudio project file

**Figures:** in this folder you will save the figures you need for your analysis