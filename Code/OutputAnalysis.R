################################################################################
# Analyze the results from a LandClim simulation
# Author: Olalla Díaz Yáñez (olalladiaz.net)
# Aim: Obtain plots to analyze the output from the LandCLim simulations
################################################################################

# 0) Specify the name of the scenarios ----

  # First you want to define from which scenarios you want to plot the figures.
  # This is needed because you have saved the output files under different folders
  # inside the folder Outputfiles. These names were specified by you

  # You can provide a vector with all the names for the scenarios that you have run
  # as the code below is designed with a loop 
  # so you don't have to specify one by one each of them 

  # e.g. if you have run three scenarios called RCP26woBB, "RCP60wBB and RCP85woBB
     # then the list of scenarios should be:
     # ListOfScenarios <- c("RCP26woBB", "RCP60wBB", RCP85woBB") 


  # For the test run the scenario we just write "test"

    ListOfScenarios <- c("test")


# 1) Display the landscape maps ----
    
    # Specify the year of the fullout file that you want to use
    # e.g. if your simulation length was of 100 years you could check the file 
    # for the end of the simulation on the 100th year
    # You can check what files you have produced as output in the scenario folder under Outputfiles folder
    # You specified which ones you created in the Runsimulation.R script, parameter fulloutputtime 
    
    fulloutfileinuse <- 100

   
   # The following loop plots the maps for each of the scenarios in your list
   
   for (i in 1:length (ListOfScenarios)){
     
     scenario <- ListOfScenarios[i]
     
     # Load full output for specified year
     
     FullOutfile <- read.csv (paste0 ("Outputfiles/", scenario, "/fullOut_year_", 
                                     fulloutfileinuse, ".csv"), header = T)
     
     
     #For displaying the landscape maps, we first need to aggregate the data:
     
     #(1) biomass per cell, 
     #(2) number of stems per cell,
     #(3) dominant species per cell. 
     
     library (dplyr) # Required library activated
     
     #(1) Calculate biomass sum per grid-cell 
     
     full_aggregate_biomass <- FullOutfile  %>% 
                                        group_by (row, col) %>% 
                                          summarise (CellBiomass = sum (biomass)) 
     
     #(2) Calculate sum of stems per grid-cell 
     
     full_aggregate_stems <- FullOutfile  %>% 
                                    group_by (row, col) %>% 
                                        summarise(CellStems = sum (stems)) 
     
     #(3) Calculate dominant species based on tree height
     
     dominantSpecies <- FullOutfile  %>% 
                            mutate (cohortBiomass = stems * biomass) %>% 
                                group_by (row, col, cell, species) %>% 
                                    summarise (speciesStems = sum (stems),
                                               speciesBiomass = sum (cohortBiomass), 
                                               height = max (height)) %>% 
                                                      group_by (row, col, cell) %>% 
                                                          filter (speciesStems == max (speciesStems)) %>% 
                                                              filter(speciesBiomass == max (speciesBiomass)) %>%
                                                                  filter (height == max(height)) 
     
     # Plot the distribution of tree biomass over the landscape
     
     library (ggplot2)
     TreeBiomassPlot <- ggplot(full_aggregate_biomass) + 
       ggtitle (paste0 ("Distribution of tree biomass", " on the scenario ", scenario)) +
       geom_raster (aes (x = col, y = row, fill = CellBiomass)) + 
       scale_fill_gradient(low = "lightgreen", high = "darkgreen", space = "Lab")
     
     ggsave(paste0 ("TreeBiomassPlot", scenario, ".png"),  plot = TreeBiomassPlot,
            path = "Figures/")
     
     
     # Plot number of stems per cell
     
     StemsCellPlot <- ggplot (full_aggregate_stems) + 
       ggtitle (paste0 ("Number of stems per cell", " on the scenario ", scenario)) +
       geom_raster (aes (x = col, y = row, fill = CellStems)) + 
       scale_fill_gradient (low = "lightgreen", high = "darkgreen", space = "Lab")
     
     ggsave(paste0 ("StemsCellPlot", scenario, ".png"),  plot = StemsCellPlot,
            path = "Figures/")
     
     # Plot distribution of dominant species
     
     library (RColorBrewer) 
     DomSpeciesPlot <- ggplot (dominantSpecies) + 
       ggtitle (paste0 ("Distribution of dominant species", " on the scenario ", 
                        scenario)) +
       geom_raster (aes (x = col, y = row, fill = species)) + 
       scale_fill_brewer (palette = "Spectral")  
     
     ggsave(paste0 ("DomSpeciesPlot", scenario, ".png"),  plot =  DomSpeciesPlot,
            path = "Figures/")
     
   }
   
# 2) Display the biomass succession information in a selected elevation ----
   
   
   # Provide a list of elevations from where you want the plots
   
   ListOfElevations <- c(1800, 2000, 2100)
   
   
   # The following loop plots the figures for each of the scenarios in your list
   # and for each of the elevations that you have selected
   
   for (i in 1:length (ListOfScenarios)){
     
     scenario <- ListOfScenarios[i]
     
     # load elevation biomass (in tons per ha)
     elevBiomass <- read.csv (paste0 ("Outputfiles/", scenario,
                                      "/BiomassOut_elevation.csv"), header = T)  
   
     # abbreviated species names 
     SpeciesNames <- names (elevBiomass)[2 : (ncol (elevBiomass) - 2)] 
   
     # load library required for colour palette 
     library (gplots)
     library (plotrix)
     myCols <- rev (rich.colors (length (SpeciesNames))) # create colour palette
     
     for (n in 1:length(ListOfElevations )){
       SelectElevation <- ListOfElevations [n]
       ElevBiomassSubset <- elevBiomass [elevBiomass$elevation == SelectElevation, ]
       
       png(paste0 ("Figures/BioSppAltPlot", scenario, SelectElevation, ".png"),
           width = 900, height = 600, units = 'px', res= 100)
       
       # Plot succession figure 
       stackpoly (ElevBiomassSubset [SpeciesNames], stack = T, col = myCols, 
                  main = paste ("Species succession (elevation ", SelectElevation, ")",
                                " on the scenario ", scenario, sep = ""),
                  ylab = "Biomass (tons per ha)", xlab = "Time (decades)", 
                  axis4 = F, cex.lab = 1, las = 2, cex.axis = 2, ylim = c(0, 400))
       
       # Add legend
       legend ("topright", legend = SpeciesNames, cex = 0.6, fill = myCols, ncol = 6)
       dev.off()
       
     }
   }
    
   
# 3) Plot the biomass of all species along an elevation gradient----
     
   
   # Provide a list of years from where you want the plots
   
   ListOfSelectYear <- c(10, 50, 100)
   
   # The following loop plots the figures for each of the scenarios in your list
   # and for each of the years that you have selected
   
   for (i in 1:length (ListOfScenarios)){
     
     scenario <- ListOfScenarios[i]
     
     elevBiomass <- read.csv (paste0 ("Outputfiles/", scenario, "/BiomassOut_elevation.csv"),
                              header = T) # load elevation biomass #biomass (in tons per ha) 
     
     # abbreviated species names 
     SpeciesNames <- names (elevBiomass)[2 : (ncol (elevBiomass) - 2)] 
     
     for (n in 1: length(ListOfSelectYear)){
       SelectYear <- ListOfSelectYear[n]
       
       # Select one year in the simulation 
       ElevBiomassSubsetYr <- elevBiomass [elevBiomass$year == SelectYear , ]
       
       png(paste0 ("Figures/BioSppYrPlot", scenario, SelectYear, ".png"), 
           width = 900, height = 600, units = 'px', res= 100)
       
       # Plot succession figure 
       stackpoly (x = ElevBiomassSubsetYr$elevation,
                  y =  ElevBiomassSubsetYr [SpeciesNames], stack = T, col = myCols, 
                  main = paste ("Species elevation gradient (year ", SelectYear, ")",
                                " on the scenario ", scenario, sep = ""),
                  ylab = "Biomass (tons per ha)", xlab = "Elevation (m)", 
                  axis4 = F, cex.lab = 1, las = 2, cex.axis = 2, ylim = c(0, 400))
       
       # Add legend
       legend ("topright", legend = SpeciesNames, cex = 0.6, fill = myCols, ncol = 6)
       dev.off()
       
     }
   }
  

   
   
   
   
   
   
   
   
   
   
   
   
   
   
   